"""
Serveur FLASK.

For run
* playlists,
* podcasts or
* radios
on the Google Home
Version : 0.1.0
"""
import time
import sys
import logging
import re
# import random
import subprocess

# import pychromecast
from gtts import gTTS
# from slugify import slugify
from flask import Flask, request, render_template
# from flask_sslify import SSLify
from urllib.parse import urlparse
from pathlib import Path
import feedparser
import json
from song import song

# create logger
logger = logging.getLogger('Google Mini Server')
logger.setLevel(logging.NOTSET)

if '--show-debug' in sys.argv:
    logging.basicConfig(level=logging.DEBUG)
if '--show-info' in sys.argv:
    logging.basicConfig(level=logging.INFO)
if '--show-warning' in sys.argv:
    logging.basicConfig(level=logging.WARNING)


# Load settings
f = open('settings.json', 'r')
content = f.read()
f.close()
# Get default settings variable
settings = json.loads(content)

app = Flask(__name__)
logging.info("Starting up chromecasts")


@app.before_request
def validate_channel_key():
    """
    Verify Token Key' header is present on each request.

    and that its value matches the channel key we got from IFTTT. If a request
    fails this check, exit.
    """
    logging.info("Validate_channel_key")
    # if host = localhost pass without token
    try:
        pattern = settings['localhost'] + ':' + settings['flask_port']
        print(pattern)
        print(request.host)
        p = re.compile(pattern, re.IGNORECASE)
        if p.match(request.host):
            logging.info('OK pour playtts')
            return
    except Exception as ex:
        logging.error('validate key : %s' % ex)
        pass
    # Verify token (control acces for IFTTT)
    try:
        token = request.json['token']
        if token == settings['token']:
            logging.info("TOKEN OK")
        else:
            logging.info("Bad token EXIT")
            exit()
    except (TypeError, Exception) as ex:
        logging.info("No token exit !! (%s)" % ex)
        exit()


@app.route('/play/<playlist>', methods=['POST', 'GET'])
def play(playlist):
    """Play mp3 of the playlist."""
    print("type : {}".format(type(playlist)))
    print("URL :  {}".format(request.url))
    regex = r"http:.*play/(.*)sur(.*)"
    p = re.match(regex, request.url, re.MULTILINE)
    try:
        playlist = p.group(1).lstrip().rstrip()
        logging.info("Playlist : >%s<".format(playlist))
    except Exception as ex:
        mp3_chromecast_name = settings['chromecast_name']
        mp3_chromecast_ip = settings['chromecast_ip']
        logging.error('pas de nom de chromecast (%s)' % ex)
        pass
    # filename in lowercase and remove space at beginning
    # for fix errors in playlist name
    # Sous forme de liste pour etre compatible avec la version DLNA
    liste_lecture = []
    liste_lecture.append(playlist.lower().lstrip().rstrip())

    print("PLAYLIST : <{}>".format(liste_lecture[0]))
    print("il faut lancer la playlist : >{}<".format(liste_lecture[0]))
    subprocess.Popen(["python3", "lance_playlist.py", "-p {}".format(liste_lecture[0])])
    return ("End of fct play")


@app.route('/podcast/<name_podcast>', methods=['POST', 'GET'])
def podcast(name_podcast):
    """Parse podcast file and play mp3."""
    print("==============================================")
    logging.info('PODCAST : %s ' % name_podcast.lstrip())
    print('PODCAST : %s ' % name_podcast.lstrip())
    print("==============================================")
    logging.info('Podcast {}'.format(name_podcast.lstrip()))
    subprocess.Popen(["python3", "run_podcast.py", "-p {}".format(name_podcast.lstrip())])
    return "End of podcast"


@app.route('/radio/<radio>', methods=['POST'])
def play_radio(radio):
    """Play radio live link."""
    logging.info("Radio : %s" % radio.lstrip())
    content = request.get_json(silent=True)
    # print("Radio JSON Message: %s " % json.dumps(request.json))
    url = 'Erreur Radio'
    f = open('radios.json', 'r')
    content = f.read()
    f.close()
    j = json.loads(content)
    logging.info('Radio : %s ' % radio.lstrip())
    p = re.compile(radio.lstrip(), re.IGNORECASE)
    for tsf in j:
        logging.info(j[tsf]['titre'])
        logging.info(j[tsf]['url'])
        if p.match((j[tsf]['titre'])):
            logging.info("URL : %s " % j[tsf]['url'])
            url = j[tsf]['url']
            titre = j[tsf]['titre']
            break
    if url != "Erreur Radio":
        logging.info('Connexion au chromecast... %s' % settings['chromecast_name'])
        (mc, cast) = init_chromecast(settings['chromecast_name'], chromecast_ip_default)
        logging.info('On écoute la radio : %s' % radio.lstrip())
        ch = song('10', titre, url)
        mc.play_media(ch.get_url(), 'audio/mp3', ch.get_titre())
        mc.block_until_active()
        time.sleep(10)
    return radio


@app.route('/static/<path:path>')
def send_static(path):
    """Send url from a directory path."""
    return send_from_directory('static', path)


# @app.route('/conky/', methods=['POST','GET' ])
@app.route('/conky/', methods=['POST','GET' ])
def conky():
    """Message for status conky."""
    logging.info("CONKY")
    try:
        (mc, cast) = init_chromecast(settings['chromecast_name'])
        logging.info(cast.status)
        status = cast.status
        volume_level = "{:.2f} %".format(float(status.volume_level)*100)
        file = open('current_song.txt','r')
        title_current_song = file.readlines()
        file.close()
    # print(title_current_song[0])
    # logging.info(cast.media_controller.status)
        cast.quit_app()
    except:
        logging.debug('Error init Cast')
    # return jsonify(volume_level=cast.status.volume_level, standby=cast.status.is_stand_by)
    return render_template('conky.html',
                           volume_level=volume_level,
                           friendly_name=cast.device.friendly_name,
                           current_song=title_current_song[0])

if __name__ == '__main__':
        app.run(debug=settings['debug'],
                host=settings['flask_ip'],
                port=int(settings['flask_port']),
                ssl_context=('keys/fullchain.pem', 'keys/privkey.pem'))
