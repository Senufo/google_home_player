#!/usr/bin/python3
"""
Run podcast on Google Home
"""
import logging
import json
import pychromecast
import time
import re
import random
import argparse
import feedparser

from pathlib import Path

from pymediainfo import MediaInfo
from song import song

global mc
global cast

def init_chromecast(name_chromecast, ip_chromecast=None):
    """Initialize devices (google home or chromecast)."""
    try:
        cast = pychromecast.Chromecast(ip_chromecast)
        logger.info('Chromecast : %s trouve, ip : %s ' % (name_chromecast, ip_chromecast))
    except Exception as ex:
        logger.error('Problem initialize chromecast : %s' % ex)
        logger.warning("Device %s no Found, IP : %s " % (name_chromecast, settings['chromecast_ip']))
        logger.info('Search chromecasts on network')
        casts = pychromecast.get_chromecasts()
        if len(casts) == 0:
            logger.warning("No Devices Found")
            exit()
        for mon_google in casts:
            logger.warning("Cast : %s, %s" % (mon_google.device.friendly_name, name_chromecast))
            if (mon_google.device.friendly_name.upper() == name_chromecast.upper()):
                cast = mon_google
        if cast:
            logger.info('Find CAST OK')
        else:
            logger.warning("No Devices Found")
            exit()
    logger.info(cast.device)
    time.sleep(1)
    logger.info(cast.status)
    logger.info(cast.media_controller.status)
    if cast.status.app_id:
        logger.info("Killing existing app... %s" % cast.status.app_id)
        cast.quit_app()
        time.sleep(5)
    mc = cast.media_controller
    return (mc, cast)

def play_podcast(ch, mc, cast):
    """Play mp3 url on Google Home."""
    logger.info('Song URL : {}'.format(ch.get_url()))
    cast.wait()
    metadata_mp3 = ch.metadata()
    mc.play_media(ch.get_url(), 'audio/mp3', title=metadata_mp3['title'],metadata=metadata_mp3)
    mc.block_until_active()
    time.sleep(10)
    while True:
        try:
            mc.update_status()
        except Exception as ex:
            logger.error('Erreur mc.update_status : %s' % ex)
            break
        try:
            remaining = (mc.status.duration - mc.status.current_time)
        except Exception as ex:
            # Something went wrong playing the file, try the next one.
            logger.error("Invalid status, playing next... (%s)" % ex)
            break
        if mc.status.player_is_paused:
            # The Chromecast was paused for some reason, ruining the illusion
            if cast.status.app_id:
                logger.info("En pause on quitte... %s" % cast.status.app_id)
                cast.quit_app()
                time.sleep(5)
                break
        elif mc.status.player_is_idle:
            # The player stopped for some reason, play next.
            logger.info("Playing next...")
            break

        logger.info("%s seconds until next..." % (remaining - 4))
        logger.info('status media Controller : {}'.format(mc.status))
        if remaining <= 4:
            # The mp3 is almost done, play the next one.
            break
        time.sleep(4)

def run_podcast_file(name_podcast):
    """Parse podcast file and play mp3."""
    url = 'Erreur Podcast'
    # Open file with podcast links
    f = open('podcast.json', 'r')
    content = f.read()
    f.close()
    j = json.loads(content)
    logging.info('PODCAST : %s ' % name_podcast.lstrip())
    # Search for the requested podcast
    p = re.compile(name_podcast.lstrip(), re.IGNORECASE)
    for podcast in j:
        logging.info(j[podcast]['titre'])
        logging.info(j[podcast]['url'])
        if p.match((j[podcast]['titre'])):
            d = feedparser.parse(j[podcast]['url'])
            e = d.entries[0]
            logging.info("URL : %s " % e.enclosures[0]['href'])
            url = e.enclosures[0]['href']
            titre = j[podcast]['titre']

    if url != "Erreur Podcast":
        logging.info('Connexion chromecast : %s' % (settings['chromecast_name']))
        (mc, cast) = init_chromecast(settings['chromecast_name'], settings['chromecast_ip'])
        ch = song(titre, e.enclosures[0]['href'])
        metadata_mp3 = ch.metadata()
        logger.info("CH : {}, titre : {}".format(metadata_mp3['artist'], metadata_mp3['title']))
        # logging.info('Je joue %s' % ch.get_titre())
        play_podcast(ch, mc, cast)
        logging.info("Podcast started, quit application")
        cast.quit_app()
    else:
        logging.warning('No podcast >{}<'.format(name_podcast.lstrip()))
    return url

# Load settings
f = open('settings.json', 'r')
content = f.read()
f.close()
# Get default settings variable
settings = json.loads(content)

# create logger
logger = logging.getLogger('[Run Podcast]:')
# get default logging level
if settings['loggingLevel'] == 'DEBUG':
    logger.setLevel(logging.DEBUG)
    print("Level DEBUG")
if settings['loggingLevel'] == 'INFO':
    logger.setLevel(logging.INFO)
    print("Level INFO")
if settings['loggingLevel'] == 'WARNING':
    logger.setLevel(logging.WARNING)
    print("Level WARNING")
if settings['loggingLevel'] == 'NOTSET':
    logger.setLevel(logging.NOTSET)
    print("Level NOTSET")

#Recupere les options de la cli
parser = argparse.ArgumentParser(description='Lance un podcast sur Google Home ou Chromecast')
parser.add_argument('-c','--cast', action="store", default="Salle d'eau", help='Device Cast Name ?')
parser.add_argument('-mc','--media_controller', action="store", default="", help='Media Controller ?')
parser.add_argument('-p','--podcast', action="store", default="test", help='Podcast name ?')
args = parser.parse_args()
print("ARGS : {},{},{}".format(args.cast,args.media_controller,args.podcast))
logger.info("ARGS : {},{},{}".format(args.cast,args.media_controller,args.podcast))

podcast = args.podcast
run_podcast_file(podcast)
