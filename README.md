# Google Home player

Allows you to play mp3 files on your google home or your chromecast from a playlist or rss file for podcasts.

This program works under python 3 with flask

Based on [Google Home Notifier Python] (https://github.com/harperreed/google-home-notifier-python)

It is necessary :

- pychromecast
- gtts
- slugify
- flask
- feedparser
- json


## Local configuration

1. Copy the settings-example.json file to settings.json
2. Adapt it to your Configuration

For this to work it is necessary that your server is accessible on the internet and therefore have a name for your site.

3. Find your external IP address (ie Google "what's my ip?")
  _Hint_: It is highly recommended to configure a dynDNS service of your choice.
4. You can now access your server at the following address: http: //mysiteaunhosted.info: 5000

## Configuring ifttt.com

For the Google Home it is necessary to use [IFTT] (http://iftt.com)


1. Go to [IFTTT] (http://ifttt.com)
2. Create a new applet: if _This_ then _That_
3. Choose: Google Assistance
4. Choose _Say a phrase with a text
5. In _What do you want to say? _ Enter for example:

      `Diffuse $`

6. In _What do you want the Wizard to say in response? _ Enter for example:

      `Ok I play the $` playlist

7. For _That_ choose: Webhooks
  1. Choose _Make a web request_

    In URL enter:
    `YOUR_FLASK_SERVER / play / TextField {}`

    For example, if your FLASK server at this address 'mybox.me', you must enter:

    `Https://mybox.me/play/ {TextField}`

    In Method enter:
    `POST`

    In Content Type enter:
    `application / json`

    In Body enter:
     `{" token ":" YOUR_CONNECTION_PASSWORD "}`

9. Check that your example is correct.

Now every time you say "OK Google, Playlist playlist", it should play your playlist.
_Note_: If your external IP changes, it will not work anymore

## Use

To launch the program you must type:

`python player_google_home_mini_server.py`

## TODO

- Add a configuration interface
