#!/usr/bin/python3
"""
Run playlist on Google Home
"""
import logging
import json
import pychromecast
import time
import re
import random
import argparse
from pathlib import Path

from pymediainfo import MediaInfo
from song import song

global mc
global cast

def init_chromecast(name_chromecast, ip_chromecast=None):
    """Initialize devices (google home or chromecast)."""
    try:
        cast = pychromecast.Chromecast(ip_chromecast)
        logger.info('Chromecast : %s trouve, ip : %s ' % (name_chromecast, ip_chromecast))
    except Exception as ex:
        logger.error('Problem initialize chromecast : %s' % ex)
        logger.warning("Device %s no Found, IP : %s " % (name_chromecast, settings['chromecast_ip']))
        logger.info('Search chromecasts on network')
        casts = pychromecast.get_chromecasts()
        if len(casts) == 0:
            logger.warning("No Devices Found")
            exit()
        for mon_google in casts:
            logger.warning("Cast : %s, %s" % (mon_google.device.friendly_name, name_chromecast))
            if (mon_google.device.friendly_name.upper() == name_chromecast.upper()):
                cast = mon_google
        if cast:
            logger.info('Find CAST OK')
        else:
            logger.warning("No Devices Found")
            exit()
    logger.info(cast.device)
    time.sleep(1)
    logger.info(cast.status)
    logger.info(cast.media_controller.status)
    if cast.status.app_id:
        logger.info("Killing existing app... %s" % cast.status.app_id)
        cast.quit_app()
        time.sleep(5)
    mc = cast.media_controller
    return (mc, cast)

def play_mp3(ch, mc, cast):
    """Play mp3 url on Google Home."""
    logger.info('Song URL : {}'.format(ch.get_url()))
    cast.wait()
    metadata_mp3 = ch.metadata()
    mc.play_media(ch.get_url(), 'audio/mp3', title=metadata_mp3['title'],metadata=metadata_mp3)
    mc.block_until_active()
    time.sleep(10)
    while True:
        try:
            mc.update_status()
        except Exception as ex:
            logger.error('Erreur mc.update_status : %s' % ex)
            break
        try:
            remaining = (mc.status.duration - mc.status.current_time)
        except Exception as ex:
            # Something went wrong playing the file, try the next one.
            logger.error("Invalid status, playing next... (%s)" % ex)
            break
        if mc.status.player_is_paused:
            # The Chromecast was paused for some reason, ruining the illusion
            if cast.status.app_id:
                logger.info("En pause on quitte... %s" % cast.status.app_id)
                cast.quit_app()
                time.sleep(5)
                break
        elif mc.status.player_is_idle:
            # The player stopped for some reason, play next.
            logger.info("Playing next...")
            break

        logger.info("%s seconds until next..." % (remaining - 4))
        logger.info('status media Controller : {}'.format(mc.status))
        if remaining <= 4:
            # The mp3 is almost done, play the next one.
            break
        time.sleep(4)

def parse_m3u(playlist):
    """
    Parse m3u playlist file and extract.

    * URL, Title, Duration.
    """
    # Open playlist file
    f = open('static/'+playlist+'.m3u', 'r')
    lignes = f.read()
    f.close()
    # Initialize variables
    duree = 0
    songs = []
    # Parse m3u for get title, url and duration
    regex = r"#EXTINF:([0-9]+),(.*) - (.*)\n(http:.*)"
    matches = re.finditer(regex, lignes, re.MULTILINE)
    #Trouve de nombre de match
    for matchNum, match in enumerate(matches):
        matchNum = matchNum + 1
        # print ("Match {matchNum} was found at {start}-{end}: {match}".format(matchNum = matchNum, start = match.start(), end = match.end(), match = match.group()))
        try:
            duree = match.group(1)
            adresse = match.group(4)
            songs.append(song(duree, adresse))
        except Exception as ex:
            logger.info('error parse playlist : %s' % ex)
            pass
    return(songs)

def write_current_song(current):
    """Add current song at the beginning of file"""
    with open('current_song.txt','r') as contents:
      save = contents.read()
    with open('current_song.txt','w') as contents:
      contents.write(current)
      contents.write(" - {}".format(time.strftime("%H:%M, %d %b ")))
      contents.write("\n")
    with open('current_song.txt','a') as contents:
      contents.write(save)
    contents.close()

def lance_playlist(playlist, appareil=None):
    mp3_chromecast_name = settings['chromecast_name']
    mp3_chromecast_ip = settings['chromecast_ip']
    # log_msg(139,'Type playlist : {}'.format(type(playlist)))
    logger.info('Connexion chromecast : %s, %s' % (mp3_chromecast_name, mp3_chromecast_ip))
    (mc, cast) = init_chromecast(mp3_chromecast_name, mp3_chromecast_ip)
    mp3 = Path("./static/"+playlist[0].lstrip()+".m3u")
    logger.info('MP3 : %s' % mp3)
    if mp3.is_file():
        logger.info('Connexion chromecast : %s, %s' % (mp3_chromecast_name, mp3_chromecast_ip))
        (mc, cast) = init_chromecast(mp3_chromecast_name, mp3_chromecast_ip)
        list_songs = parse_m3u(playlist[0].lstrip())
        random.shuffle(list_songs)
        for ch in list_songs:
            logger.warning('[145] cast.media.media_controller.status {}'.format(cast.media_controller.status))
            status = cast.media_controller.status
            logger.info('media_metadata'.format(status.media_metadata))
            logger.info('app_id : %s' % cast.status.app_id)
            metadata_mp3 = ch.metadata()
            logger.info("CH : {}, titre : {}".format(metadata_mp3['artist'], metadata_mp3['title']))
            write_current_song('{} par {}'.format(metadata_mp3['title'], metadata_mp3['artist']))
            play_mp3(ch, mc, cast)
            if cast.status.app_id is None:
                logger.info('Quit loop')
                break
        cast.media_controller.stop()
        logger.info('End of playlist, quit cast')
        cast.quit_app()
        return ("End playlist %s " % playlist[0].lstrip())

# Load settings
f = open('settings.json', 'r')
content = f.read()
f.close()
# Get default settings variable
settings = json.loads(content)

# create logger
logger = logging.getLogger('[Lance Playlist]:')
# get default logging level
if settings['loggingLevel'] == 'DEBUG':
    logger.setLevel(logging.DEBUG)
    print("Level DEBUG")
if settings['loggingLevel'] == 'INFO':
    logger.setLevel(logging.INFO)
    print("Level INFO")
if settings['loggingLevel'] == 'WARNING':
    logger.setLevel(logging.WARNING)
    print("Level WARNING")
if settings['loggingLevel'] == 'NOTSET':
    logger.setLevel(logging.NOTSET)
    print("Level NOTSET")

#Recupere les options de la cli
parser = argparse.ArgumentParser(description='Lance une playlist sur Google Home ou Chromecast')
parser.add_argument('-c','--cast', action="store", default="Salle d'eau", help='Device Cast Name ?')
parser.add_argument('-mc','--media_controller', action="store", default="", help='Media Controller ?')
parser.add_argument('-p','--playlist', action="store", default="test", help='Playlist name ?')
args = parser.parse_args()
logger.info("ARGS : {},{},{}".format(args.cast,args.media_controller,args.playlist))

playlist = [args.playlist]
lance_playlist(playlist)
