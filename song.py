from pymediainfo import MediaInfo

class song:
    """Define song class for Playlist."""

    def __init__(self, lenght, adresse):
        """Init var."""
        self.duree = lenght
        self.url = adresse

    def get_duree(self):
        """Send mp3 duration."""
        return self.duree

    def get_url(self):
        """Send mp3 url."""
        return self.url

    def metadata(self):
        """Read mp3 tags to get metadata with MediaInfo"""
        media_info = MediaInfo.parse(self.url)
        data = media_info.to_data()
        print(data['tracks'][0])
        metadata = {
            'metadataType': 3,
            'albumName': '',
            'title': '',
            'albumArtist': '',
            'artist': '',
            'composer': '',
            'trackNumber': 0,
            'discNumber': 0,
            'images': [],
            'releaseDate': ''
        }
        try:
            metadata['title'] = data['tracks'][0]['title']
        except:
            metadata['title'] = data['tracks'][0]['Track name']
            pass
        try:
            metadata['artist'] = data['tracks'][0]['artists']
        except:
            try:
                metadata['artist'] = data['tracks'][0]['performer']
            except:
                metadata['artist'] = metadata['title']
                pass
        try:
            metadata['albumName'] = data['tracks'][0]['album']
        except:
            pass
        try:
            metadata['albumArtist'] = data['tracks'][0]['album_performer']
        except:
            pass
        try:
            metadata['trackNumber'] = data['tracks'][0]['track_name_position']
        except:
            pass
        try:
            metadata['releaseDate'] = data['tracks'][0]['recorded_date']
        except:
            pass
        return metadata
