# Google Home player

Vous permet de jouer des fichiers mp3 sur votre google home ou votre chromecast à partir d'un liste de lecture ou d'un fichier rss pour les podcasts.

Ce programme fonctionne sous python 3 avec flask

Basé sur [Google Home Notifier Python](https://github.com/harperreed/google-home-notifier-python)

Il faut :

-   pychromecast
-   gtts
-   slugify
-   flask
-   feedparser
-   json


## Configuration locale

1. Copiez le fichier settings-example.json en settings.json
2. Adaptez le à votre Configuration

Pour que cela fonctionne il est nécessaire que votre serveur soit accessible sur internet et donc avoir un nom pour votre site.

3. Trouvez votre adresse IP externe (c'est-à-dire Google "what's my ip?")
  _Astuce_ : Il est fortement recommandé de configurer un service dynDNS de votre choix.
4.  Vous pouvez maintenant acceder à votre serveur à l'adresse http://monsiteautohébergé.info:5000

## Configuration de ifttt.com

Pour le Google Home il faut utiliser [IFTT](http://iftt.com)


1. Allez sur [IFTTT](http://ifttt.com)
2. Créez une nouvelle applet: if _This_ then _That_
3. Choisissez : Google Assistance
4. Choisissez _Say a phrase with a text ingredient_
5.  Dans _What do you want to say?_ entrez par exemple:

      `Diffuse $`

6.  Dans _What do you want the Assistant to say in response?_ entrez par exemple:

      `Ok je diffuse la playlist $`

7.  Pour _That_ choisissez: Webhooks
8. Choisissez  _Make a web request_

    `Dans URL entrez :`
    `YOUR_FLASK_SERVER/play/{TextField}`

    `Par exemple, si votre serveur FLASK à cette adresse 'mybox.me', vous devez entrer:`

    `https://mybox.me/play/{TextField}`

    `Method: POST`

    `Content Type: application/json`

    `Body:`
    `    {"token":"YOUR_CONNECTION_PASSWORD"}`

9. Vérifiez que votre exemple est correct.

Maintenant chaque fois que vous direz "OK Google, Diffuse playlistname", il devrait jouer votre playlist.
_Note_: Si votre IP externe change, cela ne focntioonera plus

## Utilisation

Pour lancer le programme il faut taper :

`python player_google_home_mini_server.py`

## TODO

- Ajouter une interface de configuration
